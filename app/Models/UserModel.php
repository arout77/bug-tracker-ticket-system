<?php namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
	protected $table      = 'users';
    protected $primaryKey = 'user_id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $beforeInsert	= [];
    protected $afterInsert  = [];
    protected $beforeUpdate = [];
    protected $afterUpdate  = [];
    protected $afterFind  	= [];
    protected $afterDelete 	= [];
    // protected $allowedFields = ['name', 'email'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [
        'username'     => 'required|alpha_numeric_space|min_length[3]|max_length[20]',
        'email'        => 'required|valid_email|is_unique[members.email]',
        'password'     => 'required|min_length[6]|max_length[20]',
        'pass_confirm' => 'required_with[password]|matches[password]'
    ];
    protected $validationMessages = [
    	'username' => [
        	'required'   => 'Please choose a username',
        	'min_length' => 'Too short, man!',
    	],
        'email'        => [
            'is_unique' => 'Sorry. That email has already been taken. Please choose another.'
        ]
    ];
    protected $skipValidation     = false;

	public function create_new_user( Array $userData )
	{
		extract($userData);

		$session = \Config\Services::session();
		$db = \Config\Database::connect();

		$builder = $db->table('users');
		$builder2 = $db->table('users_authenticate');

		/*
		 *---------------------------------------------------------------
		 * Do some quick validation
		 *---------------------------------------------------------------
		 * Make sure the submitted data is kosher before moving on to step 2.
		 * If there is a problem, exit immediately. 
		 */

		// See if the email already exists If so, exit and let the user know
		$builder->getWhere(['email' => $email]);

		if($db->affectedRows() >= 1)
		{
			exit('<div class="alert alert-danger"><h4>This email is already in use. <br>
				If you\'ve forgotten your password, you can <a style="color: red !important; font-weight: bold;" href="'.base_url('login/forgot_pass').'">reset it.</a></h4></div>');
		}

		// See if the username already exists If so, exit and let the user know
		// $builder->getWhere(['username' => $username]);

		// if($db->affectedRows() >= 1)
		// {
		// 	exit('<div class="alert alert-danger"><h4>This username is already in use. Please choose another username.</h4></div>');
		// }

		// This is a new email, so lets save this data and proceed on to step 2 of signup
		$data = [
		    'fname' 	=> $fname,
		    'lname'  	=> $lname,
		    'email'  	=> $email,
		    'is_admin'  => $is_admin
		];
		$builder->insert($data);

		// We need their member id so that we can save their password to authentication table
		$memberid = $db->query('SELECT user_id FROM users WHERE email = "' . $email . '"');
		foreach($memberid->getResult() as $row) 
			$member_id =  $row->user_id;

		$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
		$password = substr(str_shuffle($permitted_chars), 0, 10);
		$data2 = [
		    'user_id' => $member_id,
		    'password'  => password_hash($password, PASSWORD_DEFAULT),
		    'join_date' => time()
		];
		$builder2->insert($data2);

		if($db->affectedRows() >= 1)
		{
			echo '1';
		}

		$userEmail = urlencode($email);

		$emailDeliver = \Config\Services::email();

		$emailDeliver->setFrom($session->email, $session->username);
		$emailDeliver->setTo($email);
		// $email->setCC('another@another-example.com');
		// $email->setBCC('them@their-example.com');

		$emailDeliver->setSubject('Confirm your registration');
		$emailDeliver->setMessage('Your account on the bug tracking system has been created. You have been issued the following temporary password:<br><br><strong>Temporary password: ' . $password . '<br><br>Please follow the link below to confirm your account and set a new password.<br><br>' . base_url('register/confirm/'.$userEmail) .'/');

		$emailDeliver->send();

		return;
	}

	public function check_login($email, $pass)
	{
		$db = \Config\Database::connect();
		$session = \Config\Services::session();

		// Prepare the Query
		$sql = "SELECT user_id, email FROM users WHERE email = :email:";
		$query = $db->query($sql, [
		        'email'   => $email
		]);

		if(!$query->getResult())
		{
			exit("Email does not exist");
		}

		foreach($query->getResult() as $row) 
		{
			$user_id = $row->user_id;
		}

		$sql = "SELECT password FROM users_authenticate WHERE user_id = :user_id:";
		$query = $db->query($sql, [
		        'user_id'   => $user_id
		]);

		if(!$query->getResult())
		{
			exit("User Id not found. Please contact an administrator.");
		}

		foreach($query->getResult() as $row) 
		{
			$hash = $row->password;

			if(!password_verify($pass, $hash))
			{
				exit("Incorrect password");
			} 
		}
			
		$sql = "SELECT user_id, is_admin, fname, lname FROM users WHERE user_id = :user_id:";
		$query = $db->query($sql, [
		        'user_id'   => $user_id
		]);

		foreach($query->getResult() as $row)
		{
			$sessionData = [
				'user_id' => $row->user_id,
				'first_name' => $row->fname,
				'last_name' => $row->lname,
				'username' => "{$row->fname} {$row->lname}",
				'is_admin' => $row->is_admin
			];
		}
		$session->set($sessionData);
		exit('1');
			
	}

}