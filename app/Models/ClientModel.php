<?php namespace App\Models;

use CodeIgniter\Model;

class ClientModel extends Model
{
	protected $table      = 'clients';
    protected $primaryKey = 'client_id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $beforeInsert	= [];
    protected $afterInsert  = [];
    protected $beforeUpdate = [];
    protected $afterUpdate  = [];
    protected $afterFind  	= [];
    protected $afterDelete 	= [];
    // protected $allowedFields = ['name', 'email'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [
        'username'     => 'required|alpha_numeric_space|min_length[3]|max_length[20]',
        'email'        => 'required|valid_email|is_unique[members.email]',
        'password'     => 'required|min_length[6]|max_length[20]',
        'pass_confirm' => 'required_with[password]|matches[password]'
    ];
    protected $validationMessages = [
    	'username' => [
        	'required'   => 'Please choose a username',
        	'min_length' => 'Too short, man!',
    	],
        'email'        => [
            'is_unique' => 'Sorry. That email has already been taken. Please choose another.'
        ]
    ];
    protected $skipValidation     = false;

	public function create_client( Array $userData )
	{
		extract($userData);

		$session = \Config\Services::session();
		$db = \Config\Database::connect();

		$builder = $db->table('clients');
		$builder2 = $db->table('clients_authenticate');

		/*
		 *---------------------------------------------------------------
		 * Do some quick validation
		 *---------------------------------------------------------------
		 * Make sure the submitted data is kosher before moving on to step 2.
		 * If there is a problem, exit immediately. 
		 */
		if($password != $cpassword)
		{
			exit('<div class="alert alert-danger"><h4>Password and Password Confirmation does not match.</h4></div>');
		}

		// See if the email already exists If so, exit and let the user know
		$builder->getWhere(['email' => $email]);

		if($db->affectedRows() >= 1)
		{
			exit('<div class="alert alert-danger"><h4>This email is already in use. <br>
				If you\'ve forgotten your password, you can <a style="color: red !important; font-weight: bold;" href="'.base_url('login/forgot_pass').'">reset it.</a></h4></div>');
		}

		// This is a new email, so lets save this data and proceed on to step 2 of signup
		$data = [
		    'is_client'  => 1,
		    'fname' 	=> $fname,
		    'lname'  	=> $lname,
		    'email'  	=> $email,
		    'business_name'  => $business_name
		];
		$builder->insert($data);

		// We need their member id so that we can save their password to authentication table
		$memberid = $db->query('SELECT client_id FROM clients WHERE email = "' . $email . '"');
		foreach($memberid->getResult() as $row) 
			$member_id =  $row->client_id;

		$data2 = [
		    'client_id' => $member_id,
		    'password'  => password_hash($password, PASSWORD_DEFAULT),
		    'join_date' => time()
		];
		$builder2->insert($data2);

		if($db->affectedRows() >= 1)
		{
			echo '1';
		}

		$userEmail = urlencode($email);

		$emailDeliver = \Config\Services::email();

		$emailDeliver->setFrom(getenv('company.email'), getenv('company.name'));
		$emailDeliver->setTo($email);
		// $email->setCC('another@another-example.com');
		// $email->setBCC('them@their-example.com');

		$emailDeliver->setSubject('Confirm your registration');
		$emailDeliver->setMessage('Your account with ' . getenv('company.name') . ' has been created. Please follow the link below to confirm your account. If you did not create this account, you do not need to do anything; the unconfirmed account will be deleted automatically.<br><br>' . base_url('support/confirm?email='.urlencode($userEmail)) .'/');

		$emailDeliver->send();

		return;
	}

	public function confirm_registration($email)
	{
		$db = \Config\Database::connect();

		// Prepare the Query
		$sql = "SELECT client_id FROM clients WHERE email = :email:";
		$query = $db->query($sql, [
		        'email'   => $email
		]);

		if(!$query->getResult())
		{
			exit("We have no record of this email in our system.");
		}

		foreach($query->getResult() as $row) 
		{
			$client_id = $row->client_id;
		}

		// Prepare the Query
		$sql = "UPDATE clients_authenticate SET is_confirmed = 1 WHERE client_id = :client_id:";
		$query = $db->query($sql, [
		        'client_id'   => $client_id
		]);
		
	}

	public function check_login($email, $pass)
	{
		$db = \Config\Database::connect();
		$session = \Config\Services::session();
		// unset($_SESSION['login_attempts']);
		if( !isset($_SESSION['login_attempts']) )
		{
			$loginAttempts = ['login_attempts' => 0];
			$session->set($loginAttempts);
		}

		// Prepare the Query
		$sql = "SELECT client_id, email FROM clients WHERE email = :email:";
		$query = $db->query($sql, [
		        'email'   => $email
		]);

		if(!$query->getResult())
		{
			exit("We have no record of this email in our system.");
		}

		foreach($query->getResult() as $row) 
		{
			$client_id = $row->client_id;
		}

		$sql = "SELECT password, is_confirmed, is_locked_out FROM clients_authenticate WHERE client_id = :client_id:";
		$query = $db->query($sql, [
		        'client_id'   => $client_id
		]);

		if(!$query->getResult())
		{
			exit("Client ID not found. Please contact an administrator.");
		}

		foreach($query->getResult() as $row) 
		{
			if($row->is_locked_out == 1)
			{
				$sendEmail = \Config\Services::email();

				$sendEmail->setFrom(getenv('company.email'), getenv('company.name'));
				$sendEmail->setTo($email);
				// $sendEmail->setCC('another@another-example.com');
				// $sendEmail->setBCC('them@their-example.com');

				$sendEmail->setSubject('Account Locked');
				$sendEmail->setMessage('
					<p>Your account at ' . getenv('company.name') . ' has been locked as a security precaution. You, or somebody pretending to be you, recently attempted to login but had too many failed password attempts.</p>
					<p>To unlock your account, you must first visit this link before attempting to login again:</p>
					<h4>' . base_url('support/unlock_account?id=' . $client_id . '') . '</h4>
				');

				$sendEmail->send();
				exit("Your account was locked for security reasons. We have sent you an email with instructions on unlocking the account.");
			}

			if($row->is_confirmed == 0)
			{
				$emailDeliver->setFrom(getenv('company.email'), getenv('company.name'));
				$emailDeliver->setTo($email);

				$emailDeliver->setSubject('Confirm your registration');
				$emailDeliver->setMessage('Your account with ' . getenv('company.name') . ' has been created. Please follow the link below to confirm your account. If you did not create this account, you do not need to do anything; the unconfirmed account will be deleted automatically.<br><br>' . base_url('support/confirm?email='.urlencode($email)) .'/');

				$emailDeliver->send();
				exit("You must confirm your account before you are able to access the ticket system. Please check your email for instructions.");
			}

			$hash = $row->password;

			if(!password_verify($pass, $hash))
			{

				if($session->login_attempts < 5)
				{
					// Keep track of login attempts. Lock out account after five failed attempts
					$tracker = (int) $session->login_attempts + 1;
					$session->set('login_attempts', $tracker);
					exit("Incorrect password");
				}

				if($session->login_attempts >= 5)
				{
					$sql = "UPDATE clients_authenticate
					SET is_locked_out = 1 
					WHERE client_id = :client_id:";
					$query = $db->query($sql, [
					        'client_id'   => $client_id
					]);

					if(!$query->getResult())
					{
						$sendEmail = \Config\Services::email();

						$sendEmail->setFrom(getenv('company.email'), getenv('company.name'));
						$sendEmail->setTo($email);
						// $sendEmail->setCC('another@another-example.com');
						// $sendEmail->setBCC('them@their-example.com');

						$sendEmail->setSubject('Account Locked');
						$sendEmail->setMessage('
							<p>Your account at ' . getenv('company.name') . ' has been locked as a security precaution. You, or somebody pretending to be you, recently attempted to login but had too many failed password attempts.</p>
							<p>To unlock your account, you must first visit this link before attempting to login again:</p>
							<h4>' . base_url('support/unlock_account?id=' . $client_id . '') . '</h4>
						');

						$sendEmail->send();

						exit("Too many failed login attempts. Please check your email for instructions to unlock account.");
					}
				}
			} 
		}
			
		$sql = "SELECT client_id, is_client, fname, lname FROM clients WHERE client_id = :client_id:";
		$query = $db->query($sql, [
		        'client_id'   => $client_id
		]);

		foreach($query->getResult() as $row)
		{
			$sessionData = [
				'client_id' => $row->client_id,
				'first_name' => $row->fname,
				'last_name' => $row->lname,
				'username' => "{$row->fname} {$row->lname}",
				'is_client' => $row->is_client
			];
		}
		$session->set($sessionData);
		exit('1');			
	}

	public function unlock_account($client_id)
	{
		$db = \Config\Database::connect();
		$sql = "UPDATE clients_authenticate
					SET is_locked_out = 0 
					WHERE client_id = :client_id:";
					$query = $db->query($sql, [
					        'client_id'   => $client_id
					]);
	}

}