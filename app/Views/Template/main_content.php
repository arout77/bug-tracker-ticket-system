<?php echo view('Template/header'); ?>

<?php $session = session(); ?>


<?php  if(isset($session->username)): ?>
    <body id="page-top">
        <!-- Page Wrapper -->
        <div id="wrapper">

        <?php 
            if($session->is_user == 1) 
                echo view('Template/nav-user');
            if($session->is_client == 1) 
                echo view('Template/nav-client'); 
        ?>

<?php else: ?>

    <body class="bg-gradient-primary">
        <div class="container">
          
<?php endif; ?>

<?php 
	if(is_array($content))
	{
		foreach($content as $content)
		{
			echo view($content);
		}
	} else {
		echo view($content);
	}
?>

      </div>
    </div>
  </div>

<?php  if(isset($session->username)): ?>
<?php echo view('Template/footer'); ?>
<?php endif; ?>

  <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js');?>"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url('vendor/jquery-easing/jquery.easing.min.js');?>"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url('js/sb-admin-2.min.js');?>"></script>

  <!-- Page level plugins -->
  <script src="<?php echo base_url('vendor/chart.js/Chart.min.js');?>"></script>
  
  <!-- Page level custom scripts -->
  <script src="<?php echo base_url('js/demo/chart-area-demo.js');?>"></script>
  <!-- <script src="<?php // echo base_url('js/demo/chart-pie-demo.js');?>"></script> -->

</body>
</html>