<div class="card mb-4">
	<div class="card-header py-3">
	  <h6 class="m-0 font-weight-bold text-primary">Admin Access Only</h6>
	</div>
	<div class="card-body">
	  Only system administrators are authorized to create / register new users. Please contact your system admin if you need assistance.
	</div>
</div>