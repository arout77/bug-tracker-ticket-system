<body class="bg-gradient-primary">

  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                <span id="response"></span>
              </div>
              <form class="user" id="signupForm" method="post" action="<?= base_url('register/process_form'); ?>">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" id="fname" name="fname" placeholder="First Name" required>
                  </div>
                  <div class="col-sm-6">
                    <input type="text" class="form-control form-control-user" id="lname" name="lname" placeholder="Last Name" required>
                  </div>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control form-control-user" id="email" name="email" placeholder="Email Address" required>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <label>Grant this user administrator access?</label>
                  </div>
                  <div class="col-sm-6">
                    <select class="custom-select custom-select-sm form-control form-control-sm" name="is_admin">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row" id="notice" style="display: none;">
                  <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                      <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                          <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">An email will be sent to this user with a temporary password and instructions to confirm registration.</div>
                        </div>
                        <div class="col-auto">
                          <i class="fas fa-envelope fa-2x text-gray-300"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <button class="btn btn-primary btn-user btn-block">
                  Register Account
                </button>
                
              </form>
              <hr>
              <div class="text-center">
                <a class="small" href="<?= base_url('login/forgot_pass'); ?>">Forgot Password?</a>
              </div>
              <div class="text-center">
                <a class="small" href="<?= base_url('register/index'); ?>">Already have an account? Login!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

<script type="text/javascript" src="<?= base_url('vendor/jquery/jquery.min.js');?>"></script> 
<script>
$( document ).ready(function() {
    $("#signupForm").submit(function(e){
        e.preventDefault(); //prevent default action 
        var post_url = $(this).attr("action"); //get form action url
        var request_method = $(this).attr("method"); //get form GET/POST method
        var form_data = $(this).serialize(); //Encode form elements for submission
        
        $.ajax({
            url : post_url,
            type: request_method,
            data: form_data
        }).done(function(response){ //
            console.log(response);
            if(response != '1')
            {
                $("#response").html(response);
            } else {
                $("#notice").show();
                function redir()
                {
                    window.location.href = "<?= base_url(); ?>";
                }
                setTimeout(redir, 4000);
            }
        });
    });
});
</script>