<?php echo view('Template/header'); ?>

<body>
 <style>
  .bootstrap-select.btn-group .dropdown-toggle .filter-option {
      display: inline-block;
      overflow: hidden;
      width: 100%;
      text-align: center;
  }
  </style>
<!--=================================
 preloader -->
<div id="preloader">
  <div class="clear-loading loading-effect"><img src="<?= base_url('images/loading.gif'); ?>" alt="" /></div>
</div>

<?php $session = session(); ?>

<?php 
if(isset($session->username)) 
    echo view('Template/nav-user'); 
else
    echo view('Template/nav-visitor'); 
?>

<!--=================================
 banner -->
<section id="main-slider" class="fullscreen">
  <div class="banner banner-3 bg-1 bg-overlay-red " style="background-image: url(<?= base_url('images/bg/bg-6.jpg'); ?>)">
    <div class="slider-static">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-7 col-md-12 align-self-center">
            <h1 class="animated3 text-white">choose Your <span>soul Mate</span><br>
              from 100,000+ <span> lonely hearts</span></h1>
          </div>
          <div class="col-lg-5 col-md-12 mt-5 sm-mt-0">
            <div class="banner-form">
              <h4>dating with Cupid love Your perfect match is just a click away</h4>
              <form class="form-inner">
                <div class="form-group">
                  <label class="col-md-4 control-label">I am a</label>
                  <div class="col-md-8">
                    <select class="selectpicker">
                      <option>Man</option>
                      <option>Woman</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-4 control-label">Seeking a</label>
                  <div class="col-md-8">
                    <select class="selectpicker">
                      <option>Man</option>
                      <option>Woman</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-4 control-label">From</label>
                  <div class="col-md-8">
                    <select class="selectpicker">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                      <option>6</option>
                      <option>7</option>
                      <option>8</option>
                      <option>9</option>
                      <option>10</option>
                      <option>11</option>
                      <option>12</option>
                      <option>13</option>
                      <option>14</option>
                      <option>15</option>
                      <option>16</option>
                      <option>17</option>
                      <option>18</option>
                      <option>19</option>
                      <option>20</option>
                      <option>21</option>
                      <option>22</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-4 control-label">To</label>
                  <div class="col-md-8">
                    <select class="selectpicker">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                      <option>6</option>
                      <option>7</option>
                      <option>8</option>
                      <option>9</option>
                      <option>10</option>
                      <option>11</option>
                      <option>12</option>
                      <option>13</option>
                      <option>14</option>
                      <option>15</option>
                      <option>16</option>
                      <option>17</option>
                      <option>18</option>
                      <option>19</option>
                      <option>20</option>
                      <option>21</option>
                      <option>22</option>
                    </select>
                  </div>
                </div>
                <div class="form-group mb-0 text-right col-md-12"> <a class="button btn-lg btn-theme full-rounded animated right-icn"><span>search <i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span></a> </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!--=================================
 banner --> 

<!--=================================
 Page Section --> 

<!--=================================
Step to find your Soul Mate -->

<section class="page-section-ptb text-center">
  <div class="container">
    <div class="row justify-content-center mb-5 sm-mb-3">
      <div class="col-md-8">
        <h2 class="title divider mb-3">Step to find your Soul Mate</h2>
        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 sm-mb-3">
        <div class="timeline-badge mb-2"><img class="img-center" src="images/timeline/01.png" alt="" /></div>
        <h4 class="title divider-3 mb-3">CREATE PROFILE</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  enim ad minim veniam,            quis</p>
      </div>
      <div class="col-md-4 sm-mb-3">
        <div class="timeline-badge mb-2"><img class="img-center" src="images/timeline/02.png" alt="" /></div>
        <h4 class="title divider-3 mb-3">FIND MATCH</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  enim ad minim veniam,            quis</p>
      </div>
      <div class="col-md-4">
        <div class="timeline-badge mb-2"><img class="img-center" src="images/timeline/03.png" alt="" /></div>
        <h4 class="title divider-3 mb-3">START DATING</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  enim ad minim veniam,            quis</p>
      </div>
    </div>
  </div>
</section>

<!--=================================
They found true love -->

<section class="page-section-ptb grey-bg story-slider">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8 text-center">
        <h2 class="title divider">They Found True Love</h2>
      </div>
    </div>
  </div>
  <div class="owl-carousel" data-nav-dots="true" data-items="5" data-md-items="4" data-sm-items="2"  data-space="30">
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="images/story/01.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <h5 class="title divider-3">Quinnel &amp; Jonet</h5>
          <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="images/story/02.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <h5 class="title divider-3">Adam &amp; Eve</h5>
          <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="images/story/03.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <h5 class="title divider-3">Bella &amp; Edward</h5>
          <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="images/story/04.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <h5 class="title divider-3">DEMI &amp; HEAVEN</h5>
          <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="images/story/05.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <h5 class="title divider-3">David &amp; Bathsheba</h5>
          <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="images/story/06.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <h5 class="title divider-3">Eros &amp; Psychi</h5>
          <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="images/story/07.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <h5 class="title divider-3">Hector &amp; Andromache</h5>
          <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="images/story/08.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <h5 class="title divider-3">Bonnie &amp; Clyde</h5>
          <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="images/story/09.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <h5 class="title divider-3">Henry &amp; Clare</h5>
          <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="images/story/10.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <h5 class="title divider-3">Casanova &amp; Francesca</h5>
          <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="images/story/11.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <h5 class="title divider-3">Jack &amp; Sally</h5>
          <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="story-item">
        <div class="story-image clearfix"><img class="img-fluid" src="images/story/12.jpg" alt="" />
          <div class="story-link"><a href="stories-details.html"><i class="glyph-icon flaticon-add"></i></a></div>
        </div>
        <div class="story-details text-center">
          <h5 class="title divider-3">James &amp; Lilly</h5>
          <div class="about-des mt-3">Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in</div>
        </div>
      </div>
    </div>
  </div>
</section>

<!--=================================
Last added profile -->

<section class="page-section-ptb profile-slider pb-5">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8 text-center">
        <h2 class="title divider">Last Added Profiles</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="owl-carousel" data-nav-arrow="true" data-items="4" data-md-items="4" data-sm-items="2"  data-space="30">
          <div class="item"> <a href="profile-details.html" class="profile-item">
            <div class="profile-image clearfix"><img class="img-fluid w-100" src="images/profile/01.jpg" alt="" /></div>
            <div class="profile-details text-center">
              <h5 class="title">Bill Nelson</h5>
              <span>23 Years Old</span> </div>
            </a> </div>
          <div class="item"> <a href="profile-details.html" class="profile-item">
            <div class="profile-image clearfix"><img class="img-fluid w-100" src="images/profile/02.jpg" alt="" /></div>
            <div class="profile-details text-center">
              <h5 class="title">Francisco Pierce</h5>
              <span>21 Years Old</span> </div>
            </a> </div>
          <div class="item"> <a href="profile-details.html" class="profile-item">
            <div class="profile-image clearfix"><img class="img-fluid w-100" src="images/profile/03.jpg" alt="" /></div>
            <div class="profile-details text-center">
              <h5 class="title">Nelle Townsend</h5>
              <span>19 Years Old</span> </div>
            </a> </div>
          <div class="item"> <a href="profile-details.html" class="profile-item">
            <div class="profile-image clearfix"><img class="img-fluid w-100" src="images/profile/04.jpg" alt="" /></div>
            <div class="profile-details text-center">
              <h5 class="title">Glen Bell</h5>
              <span>20 Years Old</span> </div>
            </a> </div>
          <div class="item"> <a href="profile-details.html" class="profile-item">
            <div class="profile-image clearfix"><img class="img-fluid w-100" src="images/profile/05.jpg" alt="" /></div>
            <div class="profile-details text-center">
              <h5 class="title">Bill Nelson</h5>
              <span>22 Years Old</span> </div>
            </a> </div>
          <div class="item"> <a href="profile-details.html" class="profile-item">
            <div class="profile-image clearfix"><img class="img-fluid w-100" src="images/profile/06.jpg" alt="" /></div>
            <div class="profile-details text-center">
              <h5 class="title">Francisco Pierce</h5>
              <span>23 Years Old</span> </div>
            </a> </div>
          <div class="item"> <a href="profile-details.html" class="profile-item">
            <div class="profile-image clearfix"><img class="img-fluid w-100" src="images/profile/07.jpg" alt="" /></div>
            <div class="profile-details text-center">
              <h5 class="title">Nelle Townsend</h5>
              <span>19 Years Old</span> </div>
            </a> </div>
          <div class="item"> <a href="profile-details.html" class="profile-item">
            <div class="profile-image clearfix"><img class="img-fluid w-100" src="images/profile/08.jpg" alt="" /></div>
            <div class="profile-details text-center">
              <h5 class="title">Glen Bell</h5>
              <span>22 Years Old</span> </div>
            </a> </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!--=================================
they found true love --> 

<!--=================================
Counter -->
<section class="page-section-ptb bg bg-overlay-black-30 text-white bg text-center" style="background: url(images/bg/bg-2.jpg) no-repeat 0 0; background-size: cover;">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-3 col-md-3 mb-2 text-center">
        <div class="counter fancy"> <i class="glyph-icon flaticon-people-2"></i> <span class="timer" data-to="1600" data-speed="10000">1600</span>
          <label>Total Members</label>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 text-center">
        <div class="counter fancy"> <i class="glyph-icon flaticon-favorite"></i> <span class="timer" data-to="750" data-speed="10000">750</span>
          <label>Online Members</label>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 mb-2 text-center">
        <div class="counter fancy"> <i class="glyph-icon flaticon-wedding-rings"></i> <span class="timer" data-to="380" data-speed="10000">380</span>
          <label>Men Online</label>

        </div>
      </div>
      <div class="col-lg-3 col-md-3 text-center">
        <div class="counter fancy"> <i class="glyph-icon flaticon-love-birds"></i> <span class="timer" data-to="370" data-speed="10000">370</span>
          <label>Women Online</label>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
Post Style -->

<section class="page-section-ptb">
  <div class="container">
    <div class="row justify-content-center mb-5 sm-mb-3">
      <div class="col-md-8 text-center">
        <h2 class="title divider-2 mb-3">Our Recent Blogs</h2>
        <p class="lead sm-mt-3">Nulla quis lorem ut libero malesuada feugiat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Quisque velit nisi, pretium ut lacinia in, elementum id enim.</p>
      </div>
    </div>
    <div class="row post-article">
      <div class="col-md-6">
        <div class="post right-pos">
          <div class="post-image clearfix"><img class="img-fluid" src="images/blog/v2-01.jpg" alt="" /></div>
          <div class="post-details text-left">
            <div class="post-date">27<span>MAR</span></div>
            <div class="post-meta"> <a href="#"><i class="fa fa-user"></i> Admin</a> <a href="#"><i aria-hidden="true" class="fa fa-heart-o"></i>98 Like</a> <a href="#"><i class="fa fa-comments-o"></i>Comments</a> </div>
            <div class="post-title">
              <h5 class="title text-uppercase"><a href="#">Intentions That Energize You</a></h5>
            </div>
            <div class="post-content">
              <p>Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Pellentesque in ipsum id orci porta dapibus. Curabitur aliquet quam..</p>
            </div>
            <a class="button" href="#">read more..</a> </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="post right-pos sm-mb-0">
          <div class="post-image clearfix"><img class="img-fluid" src="images/blog/v2-02.jpg" alt="" /></div>
          <div class="post-details text-left">
            <div class="post-date">27<span>MAR</span></div>
            <div class="post-meta"> <a href="#"><i class="fa fa-user"></i> Admin</a> <a href="#"><i aria-hidden="true" class="fa fa-heart-o"></i>98 Like</a> <a href="#"><i class="fa fa-comments-o"></i>Comments</a> </div>
            <div class="post-title">
              <h5 class="title text-uppercase"><a href="#">A Brief History Of Creation</a></h5>
            </div>
            <div class="post-content">
              <p>Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Pellentesque in ipsum id orci porta dapibus. Curabitur aliquet quam..</p>
            </div>
            <a class="button" href="#">read more..</a> </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!--=================================
Testimonial-->

<section class="page-section-ptb pb-130 sm-pb-6 gray-bg">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8 text-center">
        <h2 class="title divider">Testimonials</h2>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-lg-10">
        <div class="owl-carousel" data-nav-dots="true" data-items="1" data-space="40" data-loop="false" data-md-items="1" data-sm-items="1">
          <div class="item">
            <div class="testimonial left_pos">
              <div class="testimonial-avatar"> <img alt="" src="images/thumbnail/thum-1.jpg"> </div>
              <div class="testimonial-info">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</div>
              <div class="author-info"> <strong>Jack Thompson - <span>Usa</span></strong> </div>
            </div>
          </div>
          <div class="item">
            <div class="testimonial left_pos">
              <div class="testimonial-avatar"> <img alt="" src="images/thumbnail/thum-2.jpg"> </div>
              <div class="testimonial-info">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</div>
              <div class="author-info"> <strong>Miss Jorina Akter - <span>Iraq</span></strong> </div>
            </div>
          </div>
          <div class="item">
            <div class="testimonial left_pos">
              <div class="testimonial-avatar"> <img alt="" src="images/thumbnail/thum-3.jpg"> </div>
              <div class="testimonial-info">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</div>
              <div class="author-info"> <strong>Adam Cooper - <span> New york</span></strong> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="page-section-ptb">
  <div class="container">
    <div class="row justify-content-center mb-5 sm-mb-3">
      <div class="col-md-8 text-center">
        <h2 class="title divider mb-3">Our team member</h2>
        <p class="lead">Eum cu tantas legere complectitur, hinc utamur ea eam. Eum patrioque mnesarchum eu, diam erant convenire et vis. Et essent evertitur sea, vis cu ubique referrentur, sed eu dicant expetendis. Eum cu</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3 col-md-6">
        <div class="team boxed">
          <div class="team-images"> <img class="img-fluid" src="images/team/team1.png" alt="" /> </div>
          <div class="team-description">
            <h5 class="title"><a href="team-single.html">Bill Nelson</a></h5>
            <span>Founder</span>
            <p>Nam nisl lacus, dignissim ac tristique ut, scelerisque eu massa. Vestibulum ligula nunc.</p>
            <div class="team-social-icon social-icons color-hover">
              <ul>
                <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li class="social-gplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li class="social-dribbble"><a href="#"><i class="fa fa-dribbble"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="team boxed">
          <div class="team-images"> <img class="img-fluid" src="images/team/team2.png" alt="" /> </div>
          <div class="team-description">
            <h5 class="title"><a href="team-single.html">Francisco Pierce</a></h5>
            <span>Photographer</span>
            <p>Nam nisl lacus, dignissim ac tristique ut, scelerisque eu massa. Vestibulum ligula nunc.</p>
            <div class="team-social-icon social-icons color-hover">
              <ul>
                <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li class="social-gplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li class="social-dribbble"><a href="#"><i class="fa fa-dribbble"></i></a></li>
              </ul>

            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="team boxed">
          <div class="team-images"> <img class="img-fluid" src="images/team/team3.png" alt="" /> </div>
          <div class="team-description">
            <h5 class="title"><a href="team-single.html">Nelle Townsend</a></h5>
            <span>Interpreter</span>
            <p>Nam nisl lacus, dignissim ac tristique ut, scelerisque eu massa. Vestibulum ligula nunc.</p>
            <div class="team-social-icon social-icons color-hover">
              <ul>
                <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li class="social-gplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li class="social-dribbble"><a href="#"><i class="fa fa-dribbble"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="team boxed">
          <div class="team-images"> <img class="img-fluid" src="images/team/team4.png" alt="" /> </div>
          <div class="team-description">
            <div class="team-tilte">
              <h5 class="title"><a href="team-single.html">Glen Bell</a></h5>
              <span>Administrator</span> </div>
            <p>Nam nisl lacus, dignissim ac tristique ut, scelerisque eu massa. Vestibulum ligula nunc.</p>
            <div class="team-social-icon social-icons color-hover">
              <ul>
                <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li class="social-gplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li class="social-dribbble"><a href="#"><i class="fa fa-dribbble"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!--=================================
 page-section -->

<section class="page-section-ptb bg fixed text-white bg-overlay-black-50" style="background-image:url(images/bg/bg-5.jpg)">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8 text-center">
        <h2 class="title title2 divider mb-3">Get The Best
          <label>Dating</label>
          Team Now</h2>
        <h5 class="pb-20">Want to hear more story, subscribe for our newsletter</h5>
        <a class="button  btn-lg btn-theme full-rounded animated right-icn"><span>Subscribe<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span></a> </div>
    </div>
  </div>
</section>

<?php echo view('Template/footer'); ?>