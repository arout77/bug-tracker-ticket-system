<?php 
$session = \Config\Services::session(); 
if(!isset($session->username)):
?>
<p style="margin-top: 25%;"></p>
<?php endif;?>

<div class="card shadow mb-4">
	<div class="card-header py-3">
	  <h6 class="m-0 font-weight-bold text-primary"><?= getenv('company.name'); ?> Customer Support Portal</h6>
	</div>
	<div class="card-body">
	  <div class="text-center">
	    <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="img/undraw_Preparation_re_t0ce.svg" alt="">
	  </div>
	  <p>
	  	Welcome to <?= getenv('company.name'); ?>'s customer support page. If you are a previous or current client, from here you can:
	  	<div class="card border-left-primary shadow">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
              	<ul>
			  		<li>Report any bugs found in your software. If reporting more than one, please open a separate ticket for each issue.</li>
			  		<li>Request new features / upgrades to existing software.</li>
			  		<li>Interact with our team regarding existing tickets.</li>
			  	</ul>
			  </div>
            </div>
          </div>
        </div>
      </div>
	  	
	  </p>
	  <p>
	  		Please note that you must already be an existing client to use our portal. If you are not, but are experiencing issues with your website, we'd love to help! 
	  		<a href="tel:<?= getenv('company.phone'); ?>" type="tel">Give us a call</a> or <a href="mailto:<?= getenv('company.email'); ?>">send us an email</a>
	  </p>
	  <p>
	  	<div class="row">
	  		<div class="col-md-6">
				<a href="<?= base_url('support/login'); ?>" class="btn btn-primary btn-icon-split btn-md"  style="float:right;">
					<span class="icon text-white-50">
					  <i class="fas fa-lock-open"></i>
					</span>
					<span class="text">Login To Continue</span>
				</a>
			</div>
			<div class="col-md-6">
				<a href="<?= base_url('support/register'); ?>" class="btn btn-success btn-icon-split btn-md" style="float:left;">
					<span class="icon text-white-50">
					  <i class="fas fa-plus"></i>
					</span>
					<span class="text">Create Account</span>
				</a>
			</div>
		</div>
	  </p>
	</div>
</div>