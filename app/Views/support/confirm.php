<div class="card mb-4" style="margin-top: 10%;">
  <div class="card-header">
    <span id="">Account Successfully Confirmed</span>
  </div>
  <div class="card-body">
    <p>You may now login to the Issue tracking and Ticket systems now:</p>
    <p>
        <a href="<?= base_url('support/login'); ?>" class="btn btn-md btn-primary">Login</a>
    </p>
  </div>
</div>