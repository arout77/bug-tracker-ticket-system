<?php namespace App\Controllers;

class Register extends BaseController
{

	public function __construct()
	{
		if(!isset($this->session->username))
		{
			return redirect()->to('/login');
		}
	}

	public function index()
	{
		if($this->session->is_admin != '1')
		{
			$data['content'] = 'register/not_admin';
		} else {
			$data['content'] = 'register/index';
		}

		if(!isset($this->session->username))
		{
			return redirect()->to('/login');
		}
		
		return view('Template/main_content', $data);
	}

	public function process_form()
	{
		$registerModel = new \App\Models\UserModel();
		$registerModel->create_new_user($_POST);
	}

	public function confirm()
	{
		$registerModel = new \App\Models\UserModel();
		$registerModel->create_new_user($_POST);
	}

	//--------------------------------------------------------------------

}
