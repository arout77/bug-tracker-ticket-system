<?php namespace App\Controllers;

class Support extends BaseController
{
	public function __construct()
	{
		// if(!isset($this->session->username))
		// {
		// 	return redirect()->to('/login');
		// }
	}

	public function index()
	{
		if(!isset($this->session->username))
		{
			$data['content'] = 'support/landing';
		} else {
			$data['content'] = 'support/index';
		}
		
		return view('Template/main_content', $data);
	}

	/**** Login methods ****/
	public function login()
	{
		$data['content'] = 'support/login';
		// $data['is_client'] = $this->session->get('is_client');
		return view('Template/main_content', $data);
	}

	public function process_login()
	{
		extract($_POST);
		$registerModel = new \App\Models\ClientModel();
		$registerModel->check_login($email, $password);
	}
	/**** /Login methods ****/

	/**** Signup methods ****/
	public function register()
	{
		$data['content'] = 'support/register';
		// $data['is_client'] = $this->session->get('is_client');
		return view('Template/main_content', $data);
	}

	public function process_reg_form()
	{
		extract($_POST);
		$registerModel = new \App\Models\ClientModel();
		$registerModel->create_client($_POST);
	}

	public function confirm()
	{
		// User is responding to confirmation email
		$email = $_GET['email'];
		$registerModel = new \App\Models\ClientModel();
		$registerModel->confirm_registration($email);
		$data['content'] = 'support/confirm';
		return view('Template/main_content', $data);
	}
	/**** /Signup methods ****/

	public function unlock_account()
	{
		$id = $_GET['id'];
		$clientModel = new \App\Models\ClientModel();
		$clientModel->unlock_account($id);

		$data['content'] = 'support/unlock_account';
		return view('Template/main_content', $data);
	}

	//--------------------------------------------------------------------

}
