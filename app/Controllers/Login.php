<?php namespace App\Controllers;

class Login extends BaseController
{
	protected $session;

	public function __construct()
	{
		$this->session = \Config\Services::session();
	}

	public function index()
	{
		$data['content'] = 'login/index';
		return view('Template/main_content', $data);
	}

	public function logout()
	{
		$this->session->destroy();
		$data['content'] = 'login/logged_out';
		return view('Template/main_content', $data);
	}

	public function process_login()
	{
		extract($_POST);
		$loginModel = new \App\Models\UserModel();
		$loginModel->check_login($email, $password);
	}

	//--------------------------------------------------------------------

}
