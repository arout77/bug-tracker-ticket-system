<?php namespace App\Controllers;

class Home extends BaseController
{
	// public function __construct()
	// {
	// 	if(!isset($this->session->username))
	// 	{
	// 		return redirect()->to('/login');
	// 	}
	// }

	public function index()
	{
		if(isset($this->session->username))
		{
			// return redirect()->to('/support');
			$data['content'] = 'home/index';
			return view('Template/main_content', $data);
		} else {
			require_once 'landing-page.php';
		}
		

	}

	public function dev()
	{
		$devModel = new \App\Models\DevModel();
		$devModel->update_tables();

		$data['content'] = 'home/welcome_message';
		return view('Template/main_content', $data);
	}

	//--------------------------------------------------------------------

}
