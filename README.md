# Issue Tracking and Live Support Ticket System #

Built on top of CodeIgniter version 4.0, this is a fully featured issue tracking system to help your development team prioritize and collaborate on tasks. 
It can be used as a private in-house system, or you can make available a ticket system for your clients to report bugs, upgrade requests and other issues.

### THIS SOFTWARE IS UNDER DEVELOPMENT AND NOT YET SUITABLE FOR PRODUCTION ###

Please do not download and try to use this just yet. A lot of features are still being tested and not yet implemented. There may be bugs. Documentation 
has not been written yet.
A notice will be displayed here when a beta and production version is ready. Beta is currently expected to be ready in mid-October, production should be ready before the 
end of the year.

### Who do I talk to? ###

If you have any questions, comments or death threats, please send them to me at arout@diamondphp.org.